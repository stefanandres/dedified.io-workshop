# Persistent storage

```sh
# Check default storage class
kubectl get storageclasses.storage.k8s.io

# Create STS
kubectl apply -f statefulset.yaml

# See progress
kubectl get pod,pv,pvc
kubectl get pod web-0 -o yaml

kubectl delete -f statefulset.yaml

# PVCs (and PVs) remain until manually deleted
kubectl get pv,pvc
kubectl delete pvc -l app=nginx
kubectl get pv
```
