# sidecars

## InitContainers

```sh
# Create pod
kubectl apply -f never-finishes-pod-initcontainers.yaml

# Check logs of pod
kubectl get pod
kubectl logs myapp-pod
kubectl logs myapp-pod --all-containers

# Delete pod
kubectl delete -f never-finishes-pod-initcontainers.yaml
```

## Running Sidecars

```sh
# Create container
kubectl apply -f sidecar-pod.yaml
kubectl get pod

# Exec into single container
kubectl exec -i myapp-pod -c <CONTAINER_NAME> sh

# Put data in shared in volume
kubectl exec -it myapp-pod -c myapp-sidecar sh
ls -la /cache/
touch /cache/1

kubectl exec -it myapp-pod -c myapp-container sh
ls -la /cache/

# Delete pod
kubectl delete -f sidecar-pod.yaml

```
